from django.shortcuts import render

# Create your views here.


def home_view(request, *args, **kwargs):
    return render(request, "Homepage.html", {})


def me_view(request, *args, **kwargs):
    return render(request, "Me.html", {})


def whyme_view(request, *args, **kwargs):
    return render(request, "whyme.html", {})


def contact_view(request, *args, **kwargs):
    return render(request, "contact.html", {})


def secretgarden_view(request, *args, **kwargs):
    return render(request, "thanks.html", {})
